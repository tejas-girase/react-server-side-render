"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SpeakerCard = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _react = _interopRequireWildcard(require("react"));

var _link = _interopRequireDefault(require("next/link"));

var _reactPlaceholder = _interopRequireDefault(require("react-placeholder"));

var _placeholders = require("react-placeholder/lib/placeholders");

// import 'react-placeholder/lib/reactPlaceholder.css'
var SpeakerCard =
/*#__PURE__*/
function (_Component) {
  (0, _inherits2["default"])(SpeakerCard, _Component);

  function SpeakerCard() {
    (0, _classCallCheck2["default"])(this, SpeakerCard);
    return (0, _possibleConstructorReturn2["default"])(this, (0, _getPrototypeOf2["default"])(SpeakerCard).apply(this, arguments));
  }

  (0, _createClass2["default"])(SpeakerCard, [{
    key: "render",
    value: function render() {
      var awesomePlaceholder1 = _react["default"].createElement(_placeholders.MediaBlock, {
        color: "#E0E0E0",
        rows: 6
      });

      return _react["default"].createElement(_reactPlaceholder["default"], {
        showLoadingAnimation: true,
        delay: 500,
        ready: !this.props.isLoading,
        customPlaceholder: awesomePlaceholder1
      }, _react["default"].createElement(_react["default"].Fragment, null, _react["default"].createElement("img", {
        className: "card-img-top",
        src: "/static/speakers/Speaker-".concat(this.props.speaker.id, ".jpg")
      }), _react["default"].createElement("div", {
        className: "card-body"
      }, _react["default"].createElement(_link["default"], {
        href: {
          pathname: 'speaker',
          query: {
            speakerId: this.props.speaker.id
          }
        },
        as: "speaker/".concat(this.props.speaker.id)
      }, _react["default"].createElement("a", {
        className: "btn btn-lg btn-block btn-outline-primary"
      }, "Details")), _react["default"].createElement("h4", {
        className: "card-title"
      }, this.props.speaker.userFirstName, " ", this.props.speaker.userLastName), _react["default"].createElement("p", {
        className: "card-text"
      }, this.props.speaker.bioShort))));
    }
  }]);
  return SpeakerCard;
}(_react.Component);

exports.SpeakerCard = SpeakerCard;