"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _react = _interopRequireDefault(require("react"));

var _SpeakerCard = require("./SpeakerCard");

var _reactTestRenderer = _interopRequireDefault(require("react-test-renderer"));

var speaker = {
  id: 1
};
test('Check Speaker Component', function () {
  var component = _reactTestRenderer["default"].create(_react["default"].createElement(_SpeakerCard.SpeakerCard, {
    speaker: speaker
  }));

  var tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});