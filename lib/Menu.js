"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Menu = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _react = _interopRequireWildcard(require("react"));

var _link = _interopRequireDefault(require("next/link"));

var Menu =
/*#__PURE__*/
function (_Component) {
  (0, _inherits2["default"])(Menu, _Component);

  function Menu() {
    (0, _classCallCheck2["default"])(this, Menu);
    return (0, _possibleConstructorReturn2["default"])(this, (0, _getPrototypeOf2["default"])(Menu).apply(this, arguments));
  }

  (0, _createClass2["default"])(Menu, [{
    key: "render",
    value: function render() {
      return _react["default"].createElement("div", {
        className: "navbar navbar-expand-sm bg-dark navbar-dark"
      }, _react["default"].createElement("div", {
        className: "navbar"
      }, _react["default"].createElement("ul", {
        className: "navbar-nav"
      }, _react["default"].createElement("li", {
        className: "navbar-item"
      }, _react["default"].createElement(_link["default"], {
        href: "/"
      }, _react["default"].createElement("a", {
        className: "nav-link"
      }, "Home"))), _react["default"].createElement("li", {
        className: "navbar-item"
      }, _react["default"].createElement(_link["default"], {
        href: "/speakers"
      }, _react["default"].createElement("a", {
        className: "nav-link"
      }, "Speakers"))), _react["default"].createElement("li", {
        className: "navbar-item"
      }, _react["default"].createElement(_link["default"], {
        href: "/sessions"
      }, _react["default"].createElement("a", {
        className: "nav-link"
      }, "Sessions"))))));
    }
  }]);
  return Menu;
}(_react.Component);

exports.Menu = Menu;