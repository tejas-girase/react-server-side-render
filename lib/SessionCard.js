"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SessionCard = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _react = _interopRequireWildcard(require("react"));

var _reactPlaceholder = _interopRequireDefault(require("react-placeholder"));

require("react-placeholder/lib/reactPlaceholder.css");

var _placeholders = require("react-placeholder/lib/placeholders");

var SessionCard =
/*#__PURE__*/
function (_Component) {
  (0, _inherits2["default"])(SessionCard, _Component);

  function SessionCard() {
    (0, _classCallCheck2["default"])(this, SessionCard);
    return (0, _possibleConstructorReturn2["default"])(this, (0, _getPrototypeOf2["default"])(SessionCard).apply(this, arguments));
  }

  (0, _createClass2["default"])(SessionCard, [{
    key: "render",
    value: function render() {
      var awesomePlaceholder = _react["default"].createElement(_placeholders.TextBlock, {
        color: "#E0E0E0",
        rows: 6
      });

      return _react["default"].createElement(_reactPlaceholder["default"], {
        showLoadingAnimation: true,
        delay: 2500,
        ready: !this.props.isLoading,
        customPlaceholder: awesomePlaceholder
      }, _react["default"].createElement("div", {
        className: "card-body"
      }, _react["default"].createElement("h4", {
        className: "card-title"
      }), _react["default"].createElement("h6", {
        className: "card-title"
      }, this.props.session.speakersNamesCsv), _react["default"].createElement("p", {
        className: "card-text"
      }, this.props.session.descriptionShort)));
    }
  }]);
  return SessionCard;
}(_react.Component);

exports.SessionCard = SessionCard;