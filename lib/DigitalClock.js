"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _react = _interopRequireWildcard(require("react"));

require("./DigitalClock.css");

var DigitalClock =
/*#__PURE__*/
function (_Component) {
  (0, _inherits2["default"])(DigitalClock, _Component);

  function DigitalClock() {
    (0, _classCallCheck2["default"])(this, DigitalClock);
    return (0, _possibleConstructorReturn2["default"])(this, (0, _getPrototypeOf2["default"])(DigitalClock).apply(this, arguments));
  }

  (0, _createClass2["default"])(DigitalClock, [{
    key: "render",
    value: function render() {
      return _react["default"].createElement("div", {
        className: "clock"
      }, this.props.time);
    }
  }]);
  return DigitalClock;
}(_react.Component);

var _default = DigitalClock;
exports["default"] = _default;