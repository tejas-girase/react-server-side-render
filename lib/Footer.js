"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Footer = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _react = _interopRequireWildcard(require("react"));

var Footer =
/*#__PURE__*/
function (_Component) {
  (0, _inherits2["default"])(Footer, _Component);

  function Footer() {
    (0, _classCallCheck2["default"])(this, Footer);
    return (0, _possibleConstructorReturn2["default"])(this, (0, _getPrototypeOf2["default"])(Footer).apply(this, arguments));
  }

  (0, _createClass2["default"])(Footer, [{
    key: "render",
    value: function render() {
      return _react["default"].createElement("div", {
        className: "jumbotron text-center"
      }, _react["default"].createElement("h7", null, _react["default"].createElement("b", null, "Infostretch Code Camp 2019"), " is Hosted by Infostretch in Ahmedabad at their Rajpath club location, Rajpath club street.", _react["default"].createElement("b", null, "December 26 2019")));
    }
  }]);
  return Footer;
}(_react.Component);

exports.Footer = Footer;