"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Layout = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _React = _interopRequireWildcard(require("React"));

require("bootstrap/dist/css/bootstrap.min.css");

require("../static/site.css");

var _Menu = require("./Menu");

var _Footer = require("./Footer");

var Layout =
/*#__PURE__*/
function (_Component) {
  (0, _inherits2["default"])(Layout, _Component);

  function Layout() {
    (0, _classCallCheck2["default"])(this, Layout);
    return (0, _possibleConstructorReturn2["default"])(this, (0, _getPrototypeOf2["default"])(Layout).apply(this, arguments));
  }

  (0, _createClass2["default"])(Layout, [{
    key: "render",
    value: function render() {
      var children = this.props.children;
      return _React["default"].createElement(_React["default"].Fragment, null, _React["default"].createElement(_Menu.Menu, null), children, _React["default"].createElement(_Footer.Footer, null));
    }
  }]);
  return Layout;
}(_React.Component);

exports.Layout = Layout;