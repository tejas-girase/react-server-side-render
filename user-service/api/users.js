'use strict'
const uuid = require('uuid')
const AWS = require('aws-sdk')

AWS.config.setPromisesDependency(require('bluebird'))

const dynamoDb = new AWS.DynamoDB.DocumentClient()

module.exports.list = (event, context, callback) => {
  var params = {
    TableName: process.env.USER_TABLE,
    ProjectExpression: "id, fullname, email"
  }

  const onScan = (err, data) => {
    if (err) {
      console.log('Scan failed to load data. ERROR JSON:', JSON.stringify(err, null, 2));
      callback(err);
    } else {
      console.log('Scan succeeded');
      return callback(null, {
        statusCode: 200,
        body: JSON.stringify({users: data.Items})
      })
    }
  }

  dynamoDb.scan(params, onScan);
}

module.exports.get = (event, context, callback) => {
  const params = {
    TableName: process.env.USER_TABLE,
    Key: {
      id: event.pathParameters.id,
    }
  };

  dynamoDb.get(params).promise()
  .then(result => {
    const response = {
      statusCode: 200,
      body: JSON.stringify(result.Item)
    }
    callback(null, response)
  })
  .catch(error=> {
    console.log(error);
    callback(new Error('Couldn\'t fetch user.'))
    return;
  })
}

module.exports.submit = (event, context, callback) => {
  const requestBody = JSON.parse(event.body)
  const { fullname, email, exprience } = requestBody

  submitCandidateP(candidateInfo(fullname, email, exprience))
    .then(res => {
      callback(null, {
        statusCode: 200,
        body: JSON.stringify({
          message: `Sucessfully submited user with email ${email}`,
          candidateId: res.id,
        }),
      })
    })
    .catch(err => {
      console.log(err)
      callback(null, {
        statusCode: 500,
        body: JSON.stringify({
          message: `Unable to submit candidate with email ${email}`,
        }),
      })
    })
}

const submitCandidateP = candidate => {
  console.log('Submited Candidate')

  const candidateInfo = {
    TableName: process.env.USER_TABLE,
    Item: candidate,
  }

  return dynamoDb
    .put(candidateInfo)
    .promise()
    .then(res => candidate)
}

const candidateInfo = (fullname, email, exprience) => {
  const timestamp = new Date().getTime()
  return {
    id: uuid.v1(),
    fullname: fullname,
    email: email,
    exprience: exprience,
    submittedAt: timestamp,
    updatedAt: timestamp,
  }
}
