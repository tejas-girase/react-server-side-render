import React, { Component } from 'react'
import ReactPlaceholder from 'react-placeholder'
import 'react-placeholder/lib/reactPlaceholder.css'
import { MediaBlock, TextBlock } from 'react-placeholder/lib/placeholders'

export class SessionCard extends Component {
  render() {
    const awesomePlaceholder = <TextBlock color="#E0E0E0" rows={6} />
    return (
      <ReactPlaceholder
        showLoadingAnimation
        delay={2500}
        ready={!this.props.isLoading}
        customPlaceholder={awesomePlaceholder}
      >
        <div className="card-body">
          <h4 className="card-title" />
          <h6 className="card-title">{this.props.session.speakersNamesCsv}</h6>
          <p className="card-text">{this.props.session.descriptionShort}</p>
        </div>
      </ReactPlaceholder>
    )
  }
}
