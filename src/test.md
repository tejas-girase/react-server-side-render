# Building and Testing Angular

This document describes how to set up your development environment to build and test Angular.
It also explains the basic mechanics of using `git`, `node`, and `yarn`.

* [Prerequisite Software](#prerequisite-software)
* [Style Guide](#installing-npm-modules)
* [Serve Application](#serve-application)
* [Unit Tests](#unit-tests)
* [End-to-end Tests](#end-to-end-tests)



## Prerequisite Software

Before you can build and test Angular, you must install and configure the
following products on your development machine:

* [Git](http://git-scm.com) and/or the **GitHub app** (for [Mac](http://mac.github.com) or
  [Windows](http://windows.github.com)); [GitHub's Guide to Installing
  Git](https://help.github.com/articles/set-up-git) is a good source of information.

* [Node.js](http://nodejs.org), (version specified in the engines field of [`package.json`](../package.json)) which is used to run a development web server,run tests, and generate distributable files.

* [Yarn](https://yarnpkg.com) (version specified in the engines field of [`package.json`](../package.json)) which is used to install dependencies.

* [VSCode](https://code.visualstudio.com/)(Visual Studio Code is a source-code editor developed by Microsoft for Windows, Linux and macOS. It includes support for debugging, embedded Git control and GitHub, syntax highlighting, intelligent code completion, snippets, and code refactoring),

## Style Guide

https://angular.io/guide/styleguide


## Serve Application

To build Angular run:

```shell
ng serve
```

## Unit Tests
TypeScript unit-tests are usually in the `src/app` folder. Their filenames must end in `.spec.ts`.

Look for the example `src/app/app.component.spec.ts`.
Add more `.spec.ts` files as you wish; we configured karma to find them.

Run it with `npm test`

That command first compiles the application, then simultaneously re-compiles and runs the karma test-runner.
Both the compiler and the karma watch for (different) file changes.

Shut it down manually with `Ctrl-C`.

Test-runner output appears in the terminal window.
We can update our app and our tests in real-time, keeping a weather eye on the console for broken tests.
Karma is occasionally confused and it is often necessary to shut down its browser or even shut the command down (`Ctrl-C`) and
restart it. No worries; it's pretty quick.

## End-to-end Tests

E2E tests are in the `e2e` directory, side by side with the `src` folder.
Their filenames must end in `.e2e-spec.ts`.

Look for the example `e2e/app.e2e-spec.ts`.
Add more `.e2e-spec.js` files as you wish (although one usually suffices for small projects);
we configured Protractor to find them.

Thereafter, run them with `npm run e2e`.

That command first compiles, then simultaneously starts the `lite-server` at `localhost:8080`
and launches Protractor.  

The pass/fail test results appear at the bottom of the terminal window.
A custom reporter (see `protractor.config.js`) generates a  `./_test-output/protractor-results.txt` file
which is easier to read; this file is excluded from source control.

Shut it down manually with `Ctrl-C`.


## Getting the Sources

Create New Angular Project By:

1. Clone your fork of the Angular repository.

```shell
# Clone your GitHub repository:
ng new project-name

```

## Helpful Links

- [ES2015 Syntax](https://babeljs.io/learn-es2015/)
- [Angular Official Documentation] (https://angular.io)
- [Angular CLI Official Documentation] (https://angular.io/cli)
- [Angular Style guide Official Documentation] (https://angular.io/guide/styleguide)
- [Jest Documentation](https://facebook.github.io/jxest/docs/en/getting-started.html)
- [Infostretch Design Pattern Library](https://confluence.infostretch.com/display/mobile/Angular)
- [Angular tutorial for reference] (https://angular-2-training-book.rangle.io/)
- [Angular tutorial for reference] (https://devdocs.io/angular~2/guide/learning-angular)
