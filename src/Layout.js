import React, { Component } from 'React'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../static/site.css'
import { Menu } from './Menu'
import { Footer } from './Footer'

export class Layout extends Component {
  render() {
    const { children } = this.props
    return (
      <React.Fragment>
        <Menu />
        {children}
        <Footer />
      </React.Fragment>
    )
  }
}
