import React from 'react'
import renderer from 'react-test-renderer';
import { SpeakerCard } from './SpeakerCard';

const speaker = {
  id: 1,
}
test('Check Speaker Component', () => {
  const component = renderer.create(<SpeakerCard speaker={speaker} />)

  const tree = component.toJSON()
  expect(tree).toMatchSnapshot()
})
