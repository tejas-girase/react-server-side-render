import React, { Component } from 'react'

export class Footer extends Component {
  render() {
    return (
      <div className="jumbotron text-center">
        <h7>
          <b>Infostretch Code Camp 2019</b> is Hosted by Infostretch in Ahmedabad at their Rajpath
          club location, Rajpath club street.
          <b>December 26 2019</b>
        </h7>
      </div>
    )
  }
}
