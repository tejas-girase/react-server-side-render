import React, { Component } from 'react'
import './DigitalClock.css'

class DigitalClock extends Component {
  render() {
    return <div className="clock">{this.props.time}</div>
  }
}

export default DigitalClock
