import React from 'react'
function TableComponent({ rowHeaders, rowData }) {
  return (
    <div className="row">
      <div className="col-lg-2">
        <div className="margin-top-50">{getColorDivs(rowData)}</div>
      </div>
      <div className="col-lg-10">
        <table className="table table-bordered">
          <thead className="text-center">{getHeader(rowHeaders)}</thead>
          <tbody>{getRowsData(rowData)}</tbody>
        </table>
      </div>
    </div>
  )
}

function getColorDivs(data) {
  let total = 100/data.length;
  let sizeDiv = 249;
  let RED=0 ;
  let AMBER=0;
  let GREEN=0;
  for (let i = 0; i < data.length; i++) {
    const element = data[i];
    if (element.overallHealth === 'RED') {
      RED = RED + total; 
    } else if (element.overallHealth === 'AMBER') {
      AMBER = AMBER + total;
    }
    else if (element.overallHealth === 'GREEN') {
      GREEN = GREEN + total;
    }
  }
  return (<div>
        <div style={{height : sizeDiv * RED/100}}>
          <span>{RED + '%'}</span>
          <span className="color-red-icon red-color"> </span>
        </div>
        <div style={{height : sizeDiv * AMBER/100}}>
          <span>{AMBER + '%'}</span>
          <span className="color-red-icon amber-color"> </span>
        </div>
        <div style={{height : sizeDiv * GREEN/100}}
        >
          <span>{GREEN + '%'}</span>
          <span className="color-red-icon green-color"> </span>
        </div>
  </div>)
  console.log(RED, AMBER, GREEN);
}

function getHeader(data) {
  return (
    <tr className="table-active">
      <th>My Project</th>
      <th>Project Type</th>
      <th>Delivery Manager</th>
      <th>Project End Date</th>
    </tr>
  )
}

const getRowsData = data => {
  return data.map((key, index) => {
    return (
      <tr key={index + 1}>
        <td>
          {getColor(data[index].overallHealth)}
          <span className="ml12px">{data[index].projectName}</span>
        </td>
        <td>{data[index].projectType}</td>
        <td>{data[index].deliveryManager}</td>
        <td>{data[index].projectEndDate}</td>
      </tr>
    )
  })
}

const getImages = resources => {
  return resources.map((resource, index) => {
    return <img key={index + 1} alt={resource.id} />
  })
}

const getColorIcon = (data, overallHealth) => {
  return getSquareColor(overallHealth)
}

const getSquareColor = overallHealth => {
  let className = 'green-color'
  if (overallHealth === 'RED') {
    className = 'red-color'
  } else if (overallHealth === 'AMBER') {
    className = 'amber-color'
  }
  return <td className={'border-null ' + className}></td>
}

const getColor = overallHealth => {
  let className = 'green-color'
  if (overallHealth === 'RED') {
    className = 'red-color'
  } else if (overallHealth === 'AMBER') {
    className = 'amber-color'
  }
  return <span className={'round-circle ' + className}></span>
}
export default TableComponent
