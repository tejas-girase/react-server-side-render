import React, { Component } from 'react'
import axios from 'axios'

class Speaker extends Component {
  static async getInitialProps({ query }) {
    var promise = axios
      .get(`http://localhost:4000/speakers/${query.speakerId}`)
      .then(responce => {
        return {
          success: true,
          speaker: responce.data,
        }
      })
      .catch(error => {
        return {
          success: false,
          message: error.message,
        }
      })
    return promise
  }
  constructor(props) {
    super(props)
    this.state = {
      success: props.success,
      speaker: props.speaker,
      message: props.message,
    }
  }
  render() {
    return this.state.success ? (
      <div className="container">
        <div className="row">
          <h2>
            {this.state.speaker.firstName} {this.state.speaker.lastName}
          </h2>
          <p
            className="margintopbottom20"
            dangerouslySetInnerHTML={{ __html: this.state.speaker.bio || '' }}
          ></p>
        </div>
      </div>
    ) : (
      <div></div>
    )
  }
}

export default Speaker
