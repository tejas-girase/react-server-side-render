import React from 'react'
import Speaker from './Speaker'
import renderer from 'react-test-renderer'

test('indivisual speaker component', () => {
  const component = renderer.create(<Speaker />)
  const instance = component.root.instance
  instance.setState({
    success: true,
    speaker: { firstName: 'test', lastName: 'test', bio: 'this is bio' },
  })
  let tree = component.toJSON()
  expect(tree).toMatchSnapshot()
})
