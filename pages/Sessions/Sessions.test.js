import React from 'react'
import Sessions from './Sessions'
import renderer from 'react-test-renderer'

test('List sessions component', () => {
  const component = renderer.create(<Sessions />)
  const instance = component.root.instance
  instance.setState({
    success: true,
    isLoading: false,
    speakers: [{ id: 1, firstName: 'tejas', lastName: 'Girase' }],
  })
  expect(component.toJSON()).toMatchSnapshot()
})
