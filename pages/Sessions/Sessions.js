import React, { Component } from 'react'
import axios from 'axios'
import { SessionCard } from '../../src/SessionCard'

class Sessions extends Component {
  static async getInitialProps({ req }) {
    const isServer = !!req
    if (isServer) {
      var promise = axios
        .get('http://localhost:4000/sessions')
        .then(response => {
          return {
            success: true,
            isLoading: false,
            sessions: response.data,
          }
        })
        .catch(error => {
          return {
            success: false,
            isLoading: false,
            message: error.message,
          }
        })
      return promise
    } else {
      return {
        sessions: [...Array(5)].map((_, i) => ({
          firstName: '',
          lastName: '',
          id: i,
        })),
        isLoading: true,
      }
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      success: props.success,
      isLoading: props.isLoading,
      sessions: props.sessions,
      message: props.message,
    }
  }

  componentDidMount() {
    axios
      .get('http://localhost:4000/sessions')
      .then(response => {
        this.setState({
          success: true,
          isLoading: false,
          sessions: response.data,
        })
      })
      .catch(error => {
        this.setState({
          success: false,
          isLoading: false,
          message: error.message,
        })
      })
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="card-deck">
            {this.state.sessions &&
              this.state.sessions.map(session => (
                <div className="card col-4 cardmin margintopbottom" key={session.id}>
                  <SessionCard isLoading={this.state.isLoading} session={session} />
                </div>
              ))}
          </div>
        </div>
      </div>
    )
  }
}

export default Sessions
