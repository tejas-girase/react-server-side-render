import React, { Component } from 'react'
import axios from 'axios'
import TableComponent from "../../src/TableComponent";
class Users extends Component {
  static async getInitialProps({ req }) {
    const isServer = !!req;
    if (isServer) {
      let promise = axios
        .get('http://localhost:4000/projects')
        .then(response => {
          return {
            success: true,
            isLoading: false,
            users: [],
          }
        })
        .catch(error => {
          return {
            success: false,
            isLoading: false,
            message: error.meesage,
          }
        })
      return promise
    }
  }

  componentDidMount() {
    console.log("this is called");
    axios
        .get('http://localhost:4000/projects')
        .then(response => {
          this.setState({
            success: true,
            isLoading: false,
            users: response.data,
          })
        // return {
        //   success: true,
        //   isLoading: false,
        //   users: response.data,
        // }
      })
      .catch(error => {
        return {
          success: false,
          isLoading: false,
          message: error.meesage,
        }
      })
  }

  constructor(props) {
    super(props)
    this.state = {
      success: props.success,
      users: props.users,
      isLoading: props.isLoading,
      message: props.meesage,
    }
  }

  render() {
    return (
      <div className="container">
        <div className="col-lg-12">

        {this.state.users && this.state.users.length > 0 &&
        <TableComponent rowData={this.state.users} rowHeaders={this.state.users}/>}
        </div>
      </div>
    )
  }
}

export default Users
