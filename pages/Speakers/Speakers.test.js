import React from 'react'
import Speakers from './Speakers'
import renderer from 'react-test-renderer'

test('Check Speaker Component', () => {
  const component = renderer.create(<Speakers />)
  const instance = component.root.instance
  instance.setState({
    success: true,
    isLoading: false,
    speakers: [{ id: 1, userFirstName: 'tejas', userLastName: 'Girase', bioShort: 'short bio' }],
  })
  let tree = component.toJSON()
  expect(tree).toMatchSnapshot()
})
