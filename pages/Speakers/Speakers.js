import React, { Component } from 'react'
import axios from 'axios'
import { SpeakerCard } from '../../src/SpeakerCard'

class Speakers extends Component {
  static async getInitialProps({ req }) {
    const isServer = !!req
    console.log('isServerisServerisServerisServer', isServer)
    if (isServer) {
      var promise = axios
        .get('http://localhost:4000/speakers')
        .then(responce => {
          return {
            success: true,
            isLoading: false,
            speakers: responce.data,
          }
        })
        .catch(error => {
          return {
            success: false,
            isLoading: false,
            message: error.message,
          }
        })
      return promise
    } else {
      return {
        speakers: [...Array(5)].map((_, i) => ({
          firstName: '',
          lastName: '',
          id: i,
        })),
        isLoading: true,
      }
    }
  }

  componentDidMount() {
    axios
      .get('http://localhost:4000/speakers')
      .then(responce => {
        this.setState({
          success: true,
          isLoading: false,
          speakers: responce.data,
        })
      })
      .catch(error => {
        return {
          success: false,
          isLoading: false,
          message: eoror.message,
        }
      })
  }

  constructor(props) {
    super(props)
    this.state = {
      success: props.success,
      speakers: props.speakers,
      isLoading: props.isLoading,
      message: props.message,
    }
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="card-deck">
            {this.state.speakers &&
              this.state.speakers.map(speaker => (
                <div className="card col-4 cardmin margintopbottom" key={speaker.id}>
                  <SpeakerCard isLoading={this.state.isLoading} speaker={speaker} />
                </div>
              ))}
          </div>
        </div>
      </div>
    )
  }
}

export default Speakers


