import React, { Component } from 'react'
import axios from 'axios'

class index extends Component {
  static async getInitialProps() {
    var promise = axios
      .get('http://localhost:4000/sessions')
      .then(response => {
        return {
          success: true,
          sessionData: response.data,
        }
      })
      .catch(error => {
        return {
          success: false,
          message: error.message,
        }
      })
    return promise
  }

  constructor(props) {
    super(props)
    this.state = {
      success: props.success,
      sessionData: props.sessionData,
      message: props.message,
    }
  }

  render() {
    return (
      <div>
        <div className="container">
          <div className="row">
            <div className="col margintopbottom">
              <h2>Home</h2>
              <h6 className="margintopbottom20">
                Infosterch Code Camp is a community event where developers learn from fellow
                developers.
              </h6>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default index
