## What is this?

This repo houses sharing styled React components across NC applications. It is currently used in NCC's Patient Management Engine's Global Action Panel (GAP) (as of September 2018).

[![CircleCI](https://circleci.com/gh/NavigatingCancer/react_components.svg?style=svg&circle-token=126de9af3f6c6b7693bf311116f8a6ee8db5b2d3)](https://circleci.com/gh/NavigatingCancer/react_components)

## Versioning Guidelines

### Draft a New Release

Publish a new release

> https://github.com/NavigatingCancer/react_components/releases/new

- Enter the release version like "1.1.1"
- Name the release like "Release 1.1.1".
- Don't attach binaries, this is automatic

### What version to use

Commit package.json with an updated version number before merging your changes.

This project uses [semver](https://semver.org/). In brief that means that we should bump the version when...

- **Major:** Incompatible changes to existing components are introduced
- **Minor:** New components or backwards compatible changes to existing components are introduced
- **Patch:** Bug fixes or changes to documentation are introduced

## Getting Started

### Updating NCC/GC

You made some changes to the React components code, linted them, and added tests. Great! How do you modify the app to reflect these?

1. Commit your changes
1. Run pre-commit script `npm run pre-commit`
1. Push your commit to github
1. Go to the Releases tab in github and create and publish a new release according to the Versioning Guidelines above
1. In navigatingcare-components:
   1. update `patient_mgmt/app/webpack/package.json` 'ui-components' to use the specified branch (or none, for master)
      - example: `github:NavigatingCancer/react_components#fix-dropdown-in-mobile`,
   1. run `npm install` then `npm run build` from `patient_mgmt/app/webpack` directory. It will save a file named something like `patient-mgmt.5b6ea15f1a183dda46e8.js` in the `patient_mgmt/app/webpack/dist` folder. Commit this file.
1. Rev GC to capture the NCC engine changes: `bundle update --source navigatingcare-components`
1. Deploy! 🍹

### Develop NCC locally without pushing your React Components to Github

Typically, when you're using NCC, you're using the latest version of the React Components repo, which is part of the node modules that are stored in the patient_mgmt/app/webpack directory. It's possible instead to point to a local version of the React Components.

1. From this repo, run `npm link`
2. From within `navigatingcare-components/patient_mgmt/app/webpack`, run `npm link ui-components`
3. Start or restart your local NCC server (`npm run dev` from `navigatingcare-components/patient_mgmt/app/webpack`)
4. Don't forget to point NCC patient_mgmt to localhost:8080 (in application.html.haml) instead of the bundled js when you're doing live development in this fashion. (See the NCC repo for more details on how to do this).

### Dependencies

Make sure you have a recent version of [Node.js](https://nodejs.org/en/) and [npm 5.x](https://www.npmjs.com/package/npm) installed.

### Clone the repo

```
$ git clone git@github.com:NavigatingCancer/react_components.git
$ cd react_components
```

### Install dependencies

```
$ npm install
```

### Developing with storybook

The goal of the components in this repo is for them to be developed as standalone, reusable react components that can be used in isolation in various places within the app. To encourage developing in this standalone, reusable style, we develop them within [storybook](https://storybook.js.org/).

Storybook provides a GUI for seeing changes to components interactively. Components reload automatically when changes are saved, and developers can set up "knobs" for their components in order to test state changes based on prop inputs.

To run Storybook:

```
$ npm run dev
```

After Storybook is running, navigate to `localhost:8080` in your browser. Enjoy!

### Lint Your Changes

Our lint configuration is defined in an .eslintrc.json file. It's recommended that you set your editor up to show lint errors as you are developing code. There are packages for most editors that make this possible.

You can also run the linter against all .ts and .tsx files in the src/components directory via the command line using the command:

```
$ npm run lint
```

### Run Tests

We use [jest](https://facebook.github.io/jest/) to run our tests with [enzyme](https://github.com/airbnb/enzyme) to help test our react components.

```
$ npm test # Run the test suite
$ npm test -- --watch # Run tests in watch mode
```

### React Component Naming Convention

We are standardizing on the following naming convention

- For React Components, use an index.tsx file where possible if your component is the only file in the directory. This prevents redundant naming like `src/components/TabNavItem/TabNavItem.tsx`. Prefer `src/components/TabNavItem/index.tsx`
- For React Components when more than an index file exists (i.e. if you have HOCs wrapping your component), use a `Layout.tsx` filename where possible to indicate that your component is intended to be a dumb component. (See SingleSelectField as an example)
- When more components exist in the same directory and a `Layout.tsx` filename is not descriptive enough, use CamelCase for your filename. (i.e. prefer `src/components/ActionPanel/TaskForm.tsx` over `src/components/ActionPanel/task_form.tsx`)
- If your file does not contain tsx, use just the `.ts` suffix. (A bug exists on Jenkins right now to prevent you from doing this with index files that are being imported into a test via just the `'.'` path. In those cases, use .tsx as a workaround for now.)
- Style files should be named `styles.module.css`
- Tightly couple your test file names to the file which they're testing. (i.e. a test for `index.tsx` should be called `index.test.tsx`. A snapshot test for a `Layout.tsx` file should be called `Layout.test.tsx`)

## Helpful Links

- [ES2015 Syntax](https://babeljs.io/learn-es2015/)
- [React Documentation](https://reactjs.org/docs/hello-world.html)
- [CSS Modules Documentation](https://github.com/css-modules/css-modules)
- [Jest Documentation](https://facebook.github.io/jest/docs/en/getting-started.html)
- [Enzyme Documentation](http://airbnb.io/enzyme/docs/api/)
- [Infostretch Design Pattern Library]()
