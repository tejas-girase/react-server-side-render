const express = require("express");
const next = require("next");

const LRUcache = require("lru-cache");

const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();

const ssrCache = new LRUcache({
  length: function(n, key) {
    return n.toString().length + key.toString().length;
  },
  max: 100 * 1000 * 1000,
  maxAge: 1000 * 10
});

app
  .prepare()
  .then(() => {
    const server = express();
    server.get("/speaker/:speakerId", (req, res) => {
      console.log("this is called");
      console.log("this is paramas", req.params);
      const actualPage = "/speaker";
      const queryParams = { speakerId: req.params.speakerId };
      console.log(queryParams);
      app.render(req, res, actualPage, queryParams);
    });

    const cacheRoute = ["/", "/speakers", "/sessions"];

    server.get("*", (req, res) => {
      if (cacheRoute.indexOf(req.url) > -1) {
        renderWithCache(req, res, req.url, {});
      } else {
        handle(req, res);
      }
    });

    server.listen(3000, err => {
      if (err) throw err;
      console.log("server started on http://localhost:3000/");
    });
  })
  .catch(ex => {
    console.error(ex.stack);
    process.exit(1);
  });

async function renderWithCache(req, res, pagePath, queryParams) {
  const key = getCacheKey(req);
  if (ssrCache.has(key)) {
    res.setHeader("x-cache", "HIT");
    res.send(ssrCache.get(key));
    return;
  } else {
    try {
      const html = await app.renderToHTML(req, res, pagePath, queryParams);
      if (res.statusCode !== 200) {
        render.send(html);
      }
      ssrCache.set(key, html);
      res.setHeader("x-cache", "MISS");
      res.send(html);
    } catch (e) {
      app.renderError(e, req, res, pagePath, queryParams);
    }
  }
}

function getCacheKey(req) {
  return `${req.url}`;
}
